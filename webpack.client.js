const path = require('path');

module.exports = {
	entry: './client/src/index.js',

	output: {
		filename: 'bundle.js',
		path: path.join(__dirname, 'client/dist')
	},

	module: {
		rules: [
			{ test: /(pdfkit|linebreak|fontkit|unicode|brotli|png-js).*\.js$/, loader: "transform-loader?brfs" },
			{
				test: /node_modules\/unicode-properties.*\.json$/,
				loader: 'json',
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'react', 'stage-2']
				}
			},
			{
				test: /\.css$/,
				loader: 'style-loader'
			}, {
				test: /\.css$/,
				loader: 'css-loader',
				query: {
					modules: true,
					localIdentName: '[name]__[local]___[hash:base64:5]',
					camelCase: true,
				}
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loaders: [
					'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
					'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
				]
			},
			{
				test: /\.less$/,
				use: [
					{
						loader: "style-loader"
					},
					{
						loader: "css-loader",
						options: {
							sourceMap: true,
							modules: true,
							camelCase: true,
							localIdentName: "[local]___[hash:base64:5]"
						}
					},
					{
						loader: "less-loader"
					}
				]
			}
		]
	}
}
