import {
	getPermissions,
	returnSQLResponse
} from '../util';
import moment from 'moment';
import * as STATUS_CODES from 'http-status-codes';

export const getSchools = (request, response, connection) => {

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT *
		FROM (
			SELECT schools.*,
			(
				SELECT chapters.name FROM school_registration JOIN chapters ON school_registration.chapter_id=chapters.id
					WHERE school_registration.school_id=schools.id
					ORDER BY start_date DESC
					LIMIT 1
			) AS 'chapter',
			(
				SELECT chapter_id FROM school_registration
					WHERE school_registration.school_id=schools.id
					ORDER BY start_date DESC
					LIMIT 1
			) AS 'chapter_id' FROM schools
			ORDER BY schools.name
		) AS t
		WHERE
			chapter IN ${chaptersSQL}
			OR
			id IN ${schoolsSQL}
			OR
			${hasFullAccess}`,
		returnSQLResponse(connection, response)
	);
};

export const getSchoolsByChapterId = (request, response, connection) => {
	const {ids} = request.params;
	const { startDate, endDate } = request.query;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT
			school_registration.school_id,
			school_registration.chapter_id,
			schools.name,
			schools.county,
			schools.address,
			schools.state,
			chapters.name as 'chapter_name'
		FROM (
			(
				school_registration
				INNER JOIN schools ON school_registration.school_id = schools.id
			)
			INNER JOIN chapters ON school_registration.chapter_id = chapters.id
		)
		WHERE
			school_registration.chapter_id IN (${ids})
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
			${startDate ? `AND start_date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
			${endDate ? `AND end_date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''}
		GROUP BY schools.id`,
		returnSQLResponse(connection, response)
	);
};

export const getUniqueSchoolIdsFromChapterIds = (request, response, connection) => {
	let {ids} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT DISTINCT school_id, chapter_id 
		FROM school_registration 
		WHERE 
			chapter_id IN(${ids})
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
      			${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getSchoolsNameAndIdFromSchoolRegId = (request, response, connection) => {
	let {ids} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT schools.id, schools.name, school_registration.registration_id 
		FROM 
			schools
			INNER JOIN
			school_registration
			ON
			schools.id = school_registration.school_id
		WHERE school_registration.registration_id IN(${ids})
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getSchool = (request, response, connection) => {
	const {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT * 
		FROM schools 
		WHERE
			id=${id}
			AND (
				(
					SELECT chapter_id FROM school_registration
						WHERE school_registration.school_id=schools.id
						ORDER BY registration_id DESC
						LIMIT 1
				) IN ${chaptersSQL}
				OR
				id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getRegistrationInfoForSchool = (request, response, connection) => {
	const {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT schools.name, school_registration.start_date, school_registration.end_date, chapters.name AS 'chapter'
		FROM
			school_registration
			JOIN
			schools
			ON
			school_registration.school_id = schools.id
		 	JOIN chapters ON chapters.id = school_registration.chapter_id
		WHERE 
			school_registration.school_id = ${id}
			AND (
 				school_registration.chapter_id IN ${chaptersSQL}
       			OR
       			school_registration.school_id IN ${schoolsSQL}
       			OR
       			${hasFullAccess}
 			)`,
		returnSQLResponse(connection, response)
	)
}

export const getSchoolsForDate = (request, response, connection) => {
	const { isAdmin } = getPermissions(request);
	if (isAdmin)
		return response.status(STATUS_CODES.UNAUTHORIZED).send('You are not an admin user');

	let {date} = request.params;

	let dateObj = moment(date)
	let dateString1 = dateObj.year() + "-" + (dateObj.month() <= 6 ? "06-30" : "07-01")
	dateObj.subtract(1,'years').add(6,'months')
	let dateString2 = dateObj.year() + "-" + (dateObj.month() <= 6 ? "06-30" : "07-01");

	connection.query(
		`SELECT
			schools.name,
			school_registration.registration_id,
			schools.id,
			chapters.id AS chapter_id,
			chapters.name AS chapter_name
		FROM
			school_registration 
			JOIN schools 
			ON schools.id = school_registration.school_id
			JOIN chapters 
			ON chapters.id = school_registration.chapter_id
		WHERE 
			school_registration.start_date BETWEEN '${dateString1}' AND '${dateString2}'
			OR school_registration.end_date BETWEEN '${dateString1}' AND '${dateString2}'
			OR '${dateString1}' BETWEEN school_registration.start_date AND school_registration.end_date
			OR '${dateString2}' BETWEEN school_registration.start_date AND school_registration.end_date
			OR school_registration.start_date = '0000-00-00'
		GROUP BY schools.name`,
		returnSQLResponse(connection, response)
	)
}
