import {
	getPermissions,
	returnSQLResponse
} from '../util';

export const getDashBoard = (request, response, connection) => {
	let { start_date, end_date } = request.params;

	connection.query(`
		CALL get_dashboard('${start_date}','${end_date}')`,
		returnSQLResponse(connection, response)
	)
}

export const getRallyStories = (request, response, connection) => {

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);


	connection.query(`
		SELECT date,collected_data,id,stories AS 'data'
		FROM
			rallies
			JOIN
			school_registration
				ON rallies.schoolreg_id = school_registration.registration_id
		WHERE
			stories IS NOT NULL
			AND
			NOT TRIM(stories) = ""
			AND (
				school_registration.chapter_id IN ${schoolsSQL}
				OR
				school_registration.school_id IN ${chaptersSQL}
				OR
				${hasFullAccess}
			)
		ORDER BY date DESC LIMIT 50`,
		returnSQLResponse(connection, response)
	)
}

export const getRallyPictures = (request, response, connection) => {

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(`
		SELECT date,collected_data,id,picture_url AS 'data'
		FROM
			rallies
			JOIN
			school_registration
				ON rallies.schoolreg_id = school_registration.registration_id
		WHERE
			stories IS NOT NULL
			AND
			NOT TRIM(picture_url) = ""
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
		ORDER BY date DESC LIMIT 50`,
		returnSQLResponse(connection, response)
	)
}

export const getWitnessingStories = (request, response, connection) => {

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(`
		SELECT date,collected_data,witnessing_day.id,stories AS 'data',school_registration.school_id,schoolreg_id,schools.name AS 'school_name'
		FROM witnessing_day
			JOIN school_registration ON school_registration.registration_id=witnessing_day.schoolreg_id
			JOIN schools ON school_registration.school_id = schools.id
		WHERE
			stories IS NOT NULL
			AND
			NOT TRIM(stories) = ""
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
		ORDER BY date DESC LIMIT 50`,
		returnSQLResponse(connection, response)
	)

}

export const getWitnessingPictures = (request, response, connection) => {

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(`
		SELECT date,collected_data,id,picture_url AS 'data'
		FROM 
			witnessing_day
			JOIN
			school_registration
				ON witnessing_day.schoolreg_id = school_registration.registration_id
		WHERE
			picture_url IS NOT NULL
			AND
			NOT TRIM(picture_url) = ""
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
		ORDER BY date DESC LIMIT 50`,
		returnSQLResponse(connection, response)
	)
}
