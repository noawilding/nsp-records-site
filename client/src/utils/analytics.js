import 'whatwg-fetch';

const count = query => fetch(`/analytics/count?query=${query}`, {credentials: 'include'}).then(response => response.json());
const sum = query => fetch(`/analytics/sum?query=${query}`, {credentials: 'include'}).then(response => response.json());
const avg = query => fetch(`/analytics/avg?query=${query}`, {credentials: 'include'}).then(response => response.json());
const min = query => fetch(`/analytics/min?query=${query}`, {credentials: 'include'}).then(response => response.json());
const max = query => fetch(`/analytics/max?query=${query}`, {credentials: 'include'}).then(response => response.json());

export default {
	count,
	sum,
	avg,
	min,
	max
}
