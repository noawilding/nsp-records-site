import React from 'react';
import {
	getCurrentYearOfMinistry
} from '../../../utils/date_calculations';
import 'whatwg-fetch';
import queryString from 'query-string';
import moment from 'moment';

import Card from '../../elements/card/Card';

import WitnessingModule from '../../elements/modules/WitnessingModule';
import RalliesModule from '../../elements/modules/RalliesModule';
import SchoolInfoModule from '../../elements/modules/SchoolInfoModule';

import DatePicker from '../../elements/date-picker/DatePicker';
import CollapseButton from '../../elements/collapse-button/CollapseButton';

import analytics from '../../../utils/analytics';
import fetch from '../../../utils/fetch';

/*
===Schema For School Info===

{

  name,
  state,


}
*/

export default class SchoolDashboard extends React.Component {

	constructor(props) {
		super(props);

		let { id } = props.match.params;
		const parsed = queryString.parse(props.location.search);
		let current_ministry_year = getCurrentYearOfMinistry();

		this.state = {

			id: id,
			schoolName: "School",
			schoolState: "CA",
			schoolCounty: "LA",
			schoolAddress: "1234 La Ave",
			schoolIsEnrolled: false,
			generating: false,
			registrations: [],
			collapseAll: true,
			isLoaded: false,
			maxDate: parsed.max_date != null ? moment(parsed.max_date).format('YYYY-MM-DD') : current_ministry_year.max_date,
			minDate: parsed.min_date != null ? moment(parsed.min_date).format('YYYY-MM-DD') : current_ministry_year.min_date,
			rallies: [],
			witnessingDayCount: 0,
			rallyCount: 0,
			witnessingDays: []
		};

		this.handleMinDateStateChange = this.handleMinDateStateChange.bind(this);
		this.handleMaxDateStateChange = this.handleMaxDateStateChange.bind(this);
	}

	handleOnClick() {
		this.fetchData();
		this.updateUrl();
	}

	fetchData = () => {
		const promises = [
			fetch(`/api/rallies/school_id/${this.state.id}?startDate=${this.state.minDate}&endDate=${this.state.maxDate}`),
			fetch(`/api/witnessing_days/school/${this.state.id}?startDate=${this.state.minDate}&endDate=${this.state.maxDate}`),
			fetch(`/api/schools/${this.state.id}`),
			fetch(`/api/school_reg/${this.state.id}`),
			analytics.count(`rallies {school = ${this.state.id}}`),
			analytics.count(`witnessing {school = ${this.state.id}}`)
		]

		Promise.all(promises).then( results => {
			const school = results[2];

			this.setState({
				rallies: results[0],
				witnessingDays: results[1],
				schoolName: school[0].name,
				schoolAddress: school[0].address,
				schoolState: school[0].state,
				schoolCounty: school[0].county,
				registrations: results[3],
				rallyCount: results[4] && results[4].result,
				witnessingDayCount: results[5] && results[5].result,
				isLoaded: true
			})
		})
	}

	handleCollapseAllState() {
		this.setState(
			{
				collapseAll: !this.state.collapseAll
			}
		)
	}

	handleMinDateStateChange(event) {
		return this.setState(
			{
				minDate: event.target.value
			}
		);
	}

	handleMaxDateStateChange(event) {
		return this.setState(
			{
				maxDate: event.target.value
			}
		);
	}

	componentDidMount() {
		this.fetchData();
	}

	updateUrl() {
		window.history.pushState(null, null, `/school/${this.state.id}?min_date=${this.state.minDate}&max_date=${this.state.maxDate}`);
	}

	render() {
		return (
			<div className="chapter-dashboard">
				<Card cssClass="mdl-grid mdl-grid-centered mdl-cell--12-col">
					<span className="mdl-layout-title mdl-cell mdl-cell--2-col">{this.state.schoolName}</span>
					<div className="mdl-layout-spacer"></div>

					<DatePicker
						label="Start Date"
						inputId="min_date"
						onChange={this.handleMinDateStateChange}
						value={this.state.minDate}
					/>

					<div className="mdl-layout-spacer"></div>

					<DatePicker
						label="End Date"
						inputId="max_date"
						onChange={this.handleMaxDateStateChange}
						value={this.state.maxDate}
					/>

					<div className="mdl-layout-spacer"></div>

					<div className="mdl-cell mdl-cell--2-col">
						<button id="refresh" disabled={this.state.generating} onClick={this.handleOnClick.bind(this)} className="mdl-button mdl-js-button mdl-button--icon">
							<i className="material-icons">refresh</i>
						</button>
						<div htmlFor="refresh" className="mdl-tooltip">
							Refresh Dashboard
						</div>
					</div>
				</Card>

				<div className="mdl-grid mdl-textfield--align-right">
					<div className="mdl-cell--10-offset-desktop mdl-cell">
						<CollapseButton
							collapsed={this.state.collapseAll}
							collapsed_label="Show All"
							onClick={this.handleCollapseAllState.bind(this)}
							raised={true}
							uncollapsed_label="Hide All"
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<SchoolInfoModule
							county={this.state.schoolCounty}
							state={this.state.schoolState}
							id={this.state.id}
							name={this.state.schoolName}
							address={this.state.schoolAddress}
							registrations={this.state.registrations}
							minDate={this.state.minDate}
							maxDate={this.state.maxDate}
							collapsed={this.state.collapseAll}
							isLoaded={this.state.isLoaded}
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<RalliesModule
							all_time_count={this.state.rallyCount}
							collapsed={this.state.collapseAll}
							rallies={this.state.rallies}
							sort_by="school_name"
						/>
					</div>
				</div>

				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">
						<WitnessingModule
							all_time_count={this.state.witnessingDayCount}
							collapsed={this.state.collapseAll}
							max_date={this.state.maxDate}
							min_date={this.state.minDate}
							sort_by="name"
							witnessing_days={this.state.witnessingDays}
						/>
					</div>
				</div>
			</div>
		);
	}

}
